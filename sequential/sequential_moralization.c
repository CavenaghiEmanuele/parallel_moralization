#include <stdbool.h> 
#include "../matrix.c"


int main(int argc, char** argv){

    clock_t start = clock();
    
    int size = (int)strtol(argv[1], NULL, 10);

    bool *adj_matrix = random_matrix(size, size);
    bool *new_edges_matrix = create_matrix(size, size);
    bool *moralized_matrix = create_matrix(size, size);

    //MORALIZE ORIGINAL ADJACENCY MATRIX
    find_parents(adj_matrix, new_edges_matrix, size);
    make_graph_undirected(adj_matrix, new_edges_matrix, moralized_matrix, size);
    
    //FREE MEMORY
    delete_matrix(adj_matrix);
    delete_matrix(new_edges_matrix);
    delete_matrix(moralized_matrix);
    
    //Compute time
    clock_t diff = clock() - start;
    int msec = diff * 1000 / CLOCKS_PER_SEC;
    printf("Time taken for moralization %d seconds %d milliseconds\n", msec/1000, msec%1000);

    return 0;
}
