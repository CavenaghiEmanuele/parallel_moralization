#include <cuda.h>
#include <stdio.h>
#include "../matrix.c"


__global__ void find_parents_transposed_cuda(bool* adj_matrix, bool* new_edges_matrix, int size){
    int block_id = blockIdx.x;
    int thread_id = threadIdx.x;

    for(int row = thread_id; row < size; row = row + blockDim.x)
        for(int i = row+1; i < size; i++)
            if(adj_matrix[block_id*size+row] == 1 && adj_matrix[block_id*size+i] == 1)
                new_edges_matrix[row*size+i] = 1;
}

__global__ void transpose_square_matrix_cuda(bool* matrix, bool* transpose_matrix, int size){
    int block_id = blockIdx.x;
    int thread_id = threadIdx.x;

    for(int col = thread_id; col < size; col = col + blockDim.x)
        transpose_matrix[col*size+block_id] = matrix[block_id*size+col];
}

__global__ void initialize_matrix_cuda(bool* matrix, int rows, int colums){
    int block_id = blockIdx.x;
    int thread_id = threadIdx.x;

    for(int col = thread_id; col < colums; col = col + blockDim.x)
        matrix[block_id*rows+col] = 0;
}

__global__ void make_graph_undirected_cuda(bool *adj_matrix, bool *new_edges_matrix, bool *moralized_matrix, int size){
    int block_id = blockIdx.x;
    int thread_id = threadIdx.x;

    for(int col = thread_id; col < size; col = col + blockDim.x)
        if(adj_matrix[col*size+block_id] == 1 || adj_matrix[block_id*size+col] == 1 || new_edges_matrix[block_id*size+col] == 1 || new_edges_matrix[col*size+block_id] == 1){
            moralized_matrix[col*size+block_id] = 1;
            moralized_matrix[block_id*size+col] = 1;
        }else{
            moralized_matrix[col*size+block_id] = 0;
            moralized_matrix[block_id*size+col] = 0;
        }
} 

int calc_threads(int size){
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, 0);
    if(size < prop.maxThreadsPerBlock)
        return size;
    else
        return prop.maxThreadsPerBlock;
}


int main(int argc, char** argv){

    int size = (int)strtol(argv[1], NULL, 10);
    int block_size = size; // One block for each column in the adj matrix
    int threads_for_block = calc_threads(size);

    // HOST memory
    bool *adj_matrix;
    bool *moralized_matrix;
    adj_matrix = random_matrix(size, size);
    moralized_matrix = create_matrix(size, size);

    // DEVICE memory
    bool *dev_adj_matrix;
    bool *dev_transposte;
    bool *dev_new_edges_matrix;
    bool *dev_moralized_matrix;
    cudaMalloc((bool**) &dev_adj_matrix, size*size*sizeof(bool));
    cudaMalloc((bool**) &dev_transposte, size*size*sizeof(bool));
    cudaMalloc((bool**) &dev_new_edges_matrix, size*size*sizeof(bool));
    cudaMalloc((bool**) &dev_moralized_matrix, size*size*sizeof(bool));
    
    // Copy memory from host to device
    cudaMemcpy(dev_adj_matrix, adj_matrix, size*size*sizeof(bool), cudaMemcpyHostToDevice);

    // Run code on GPU
    transpose_square_matrix_cuda<<<block_size,threads_for_block>>>(dev_adj_matrix, dev_transposte, size);
    initialize_matrix_cuda<<<block_size,threads_for_block>>>(dev_new_edges_matrix, size, size);
    find_parents_transposed_cuda<<<block_size,threads_for_block>>>(dev_transposte, dev_new_edges_matrix, size);
    make_graph_undirected_cuda<<<block_size,threads_for_block>>>(dev_transposte, dev_new_edges_matrix, dev_moralized_matrix, size);

    // Copy memory back from device to host
    cudaMemcpy(moralized_matrix, dev_moralized_matrix, size*size*sizeof(bool), cudaMemcpyDeviceToHost);

    
    delete_matrix(adj_matrix);
    delete_matrix(moralized_matrix);

    cudaFree(dev_adj_matrix);
    cudaFree(dev_transposte);
    cudaFree(dev_new_edges_matrix);
    cudaFree(dev_moralized_matrix);

    return 0;
}
