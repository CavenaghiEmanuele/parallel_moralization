#include <stdbool.h> 
#include <openmpi/mpi.h>
#include "../matrix.c"


int main(int argc, char** argv) {

    double start_time = MPI_Wtime(); 
    
    int comm_size, rank;
    int size = (int)strtol(argv[1], NULL, 10);;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);11
    MPI_Status status;    
    int columns_for_process = size / comm_size;
    
    bool *adj_matrix;
    bool *transpose_adj_matrix;
    bool *moralized_matrix;
    bool *columns;
    bool *new_edges_matrix;
    bool *new_edges_matrix_reduced;

    if(rank == 0){
        adj_matrix = random_matrix(size, size);
        transpose_adj_matrix = transpose_square_matrix(adj_matrix, size);
        moralized_matrix = create_matrix(size, size);
        new_edges_matrix_reduced = create_matrix(size, size);
    }
    //****************************************************
    // PARALLEL SECTION -> FOR EVERY PROCESS
    //****************************************************
    columns = create_matrix(columns_for_process, size);
    MPI_Scatter(
                transpose_adj_matrix, 
                columns_for_process * size, 
                MPI_INT8_T, 
                columns, 
                columns_for_process * size,
                MPI_INT8_T,
                0,
                MPI_COMM_WORLD
                );
    
    new_edges_matrix = create_matrix(size, size);
    //Find parents pairs 
    for(int col = 0; col < columns_for_process; col++)
        for(int row = 0; row < size; row++)
            for(int i = row+1; i < size; i++)
                if(columns[col*size+row] == 1 && columns[col*size+i] == 1)
                    new_edges_matrix[row*size+i] = 1;
    
    MPI_Reduce(
                new_edges_matrix,
                new_edges_matrix_reduced,           
                size*size,
                MPI_INT8_T,
                MPI_BOR,
                0,
                MPI_COMM_WORLD
                );

    delete_matrix(columns);
    delete_matrix(new_edges_matrix);
    //****************************************************
    // END PARALLEL SECTION
    //****************************************************

    if(rank == 0){
        //Make graph undirected (and sum new_edges_matrix matrix and adj_matrix)
        make_graph_undirected(adj_matrix, new_edges_matrix_reduced, moralized_matrix, size);

        delete_matrix(transpose_adj_matrix);
        delete_matrix(new_edges_matrix_reduced);
        delete_matrix(adj_matrix);
        delete_matrix(moralized_matrix);
    }
    MPI_Finalize();

    printf( "Elapsed time is %f\n", MPI_Wtime() - start_time);

    return 0;
}
