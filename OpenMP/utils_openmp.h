#include <stdbool.h>

void find_parents_openmp(bool* adj_matrix, bool* new_edges_matrix, int size, int n_threads);
void find_parents_transpose_openmp(bool* adj_matrix, bool* new_edges_matrix, int size, int n_threads);
