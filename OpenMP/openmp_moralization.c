#include <omp.h>
#include <stdio.h>
#include "./utils_openmp.h"
#include "../matrix.c"


int main(int argc, char** argv) {

    double start_time = omp_get_wtime();
    
    //First parameter is size of matrix, second parameter is number of threads
    int size = (int)strtol(argv[1], NULL, 10);
    int n_threads = (int)strtol(argv[2], NULL, 10);;

    bool *adj_matrix = random_matrix(size, size);
    bool *new_edges_matrix = create_matrix(size, size);
    bool *moralized_matrix = create_matrix(size, size);
    
    find_parents_openmp(adj_matrix, new_edges_matrix, size, n_threads);
    make_graph_undirected(adj_matrix, new_edges_matrix, moralized_matrix, size);
    
    delete_matrix(adj_matrix);
    delete_matrix(new_edges_matrix);
    delete_matrix(moralized_matrix);

    printf("Time taken for moralization %f seconds\n", omp_get_wtime() - start_time);
}

void find_parents_openmp(bool* adj_matrix, bool* new_edges_matrix, int size, int n_threads){
    #pragma omp parallel num_threads(n_threads)
    {
        # pragma omp for
        //Find parents pairs 
        for(int col = 0; col < size; col++)
            for(int row = 0; row < size; row++)
                for(int i = row+1; i < size; i++)
                    if(adj_matrix[row*size+col] == 1 && adj_matrix[i*size+col] == 1)
                        new_edges_matrix[row*size+i] = 1;
    }
}
