#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>


bool* create_matrix(int n_row, int n_columns){
    bool *matrix = (bool*)malloc(n_row * n_columns * sizeof(bool));
    for (int i = 0; i < n_row*n_columns; i++)
        matrix[i] = 0;
    return matrix;
}

bool random_bool(int threshold){
    int num = rand() & 100;
    if(num < threshold)
        return 1;
    return 0;
}

bool* random_matrix(int n_row, int n_columns){
    bool *matrix = create_matrix(n_row, n_columns);
    srand(time(NULL));
    for(int row = 0; row < n_row; row++){
        for(int col = 0; col < n_columns; col++){
            matrix[row*n_row+col] = random_bool(30);
            if(row == col)
                matrix[row*n_row+col] = 0;
        }
    }
    return matrix;
}

void delete_matrix(bool* matrix){
    free(matrix);
}

void print_matrix(bool* matrix, int rows, int columns){
    for(int row = 0; row < rows; row++){
        for(int col = 0; col < columns; col++)
            printf("%d  ", matrix[row*rows + col]);
        printf("\n");
    }
}

bool* transpose_square_matrix(bool* matrix, int size){
    bool *transpose_matrix = create_matrix(size, size);
    for(int row = 0; row < size; row++)
        for(int col = 0; col < size; col++)
            transpose_matrix[col*size+row] = matrix[row*size+col];
    return transpose_matrix;
}

void make_graph_undirected(bool *adj_matrix, bool *new_edges_matrix, bool *moralized_matrix, int size){
    for(int row = 0; row < size; row++)
        for(int col = 0; col < size; col++)
            if(adj_matrix[col*size+row] == 1 || adj_matrix[row*size+col] == 1 || new_edges_matrix[row*size+col] == 1 || new_edges_matrix[col*size+row] == 1){
                moralized_matrix[col*size+row] = 1;
                moralized_matrix[row*size+col] = 1;
            }
}

bool equal_matrix(bool* matrix1, bool* matrix2, int size){
    for(int i = 0; i < size; i++)
        if(matrix1[i] != matrix2[i])
            return 0;
    return 1;
}

void find_parents(bool *adj_matrix, bool *new_edges_matrix, int size){
    for(int col = 0; col < size; col++)
        for(int row = 0; row < size; row++)
            for(int i = row+1; i < size; i++)
                if(adj_matrix[row*size+col] == 1 && adj_matrix[i*size+col] == 1)
                    new_edges_matrix[row*size+i] = 1;
}

void find_parents_transposed(bool *adj_matrix, bool *new_edges_matrix, int size){
    for(int col = 0; col < size; col++)
        for(int row = 0; row < size; row++)
            for(int i = row+1; i < size; i++)
                if(adj_matrix[col*size+row] == 1 && adj_matrix[col*size+i] == 1)
                    new_edges_matrix[row*size+i] = 1;
}

void test_with_sequential(bool* adj_matrix, bool* moralized_matrix, int size){

    bool *transposed = transpose_square_matrix(adj_matrix, size);
    bool *new_edges_matrix = create_matrix(size, size);
    bool *sequential_moralized_matrix = create_matrix(size, size);
    
    //MORALIZE TRANSPOSED ADJACENCY MATRIX
    find_parents_transposed(transposed, new_edges_matrix, size);
    make_graph_undirected(transposed, new_edges_matrix, sequential_moralized_matrix, size);
    
    //CHECK IF RESULTS ARE EQUAL
    bool equal = equal_matrix(sequential_moralized_matrix, moralized_matrix, size);
    printf("\nResults are equal? %d\n", equal);

    delete_matrix(transposed);
    delete_matrix(sequential_moralized_matrix);
}
